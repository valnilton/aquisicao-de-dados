# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Form.ui'
#
# Created: Wed Apr  6 10:13:17 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
import serial


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(800, 600)
        self.centralWidget = QtGui.QWidget(Form)
        self.centralWidget.setObjectName(_fromUtf8("centralWidget"))
        self.plot = PlotWidget(Form)
        self.plot.setGeometry(QtCore.QRect(20, 110, 761, 401))
        self.plot.setObjectName(_fromUtf8("plot"))
        self.frame = QtGui.QFrame(self.centralWidget)
        self.frame.setGeometry(QtCore.QRect(20, 20, 761, 81))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.comboBox = QtGui.QComboBox(self.frame)
        self.comboBox.setGeometry(QtCore.QRect(100, 25, 111, 25))
        self.comboBox.setEditable(True)
        self.comboBox.setMaxVisibleItems(3)
        self.comboBox.setMaxCount(10)
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.label = QtGui.QLabel(self.frame)
        self.label.setGeometry(QtCore.QRect(20, 30, 81, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.btnConectar = QtGui.QPushButton(self.frame)
        self.btnConectar.setGeometry(QtCore.QRect(220, 25, 61, 25))
        self.btnConectar.setCheckable(True)
        self.btnConectar.setChecked(False)
        self.btnConectar.setDefault(True)
        self.btnConectar.setFlat(False)
        self.btnConectar.setObjectName(_fromUtf8("btnConectar"))
        self.btnConectar.toggled.connect(self.conectar)
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(320, 10, 141, 16))
        self.label_2.setTextFormat(QtCore.Qt.AutoText)
        self.label_2.setWordWrap(False)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(self.frame)
        self.label_3.setGeometry(QtCore.QRect(320, 50, 131, 16))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.lblFoto = QtGui.QLabel(self.frame)
        self.lblFoto.setGeometry(QtCore.QRect(460, 52, 131, 10))
        self.lblFoto.setText(_fromUtf8(""))
        self.lblFoto.setObjectName(_fromUtf8("lblFoto"))
        self.lblLed = QtGui.QLabel(self.frame)
        self.lblLed.setGeometry(QtCore.QRect(708, 10, 31, 16))
        self.lblLed.setText(_fromUtf8(""))
        self.lblLed.setObjectName(_fromUtf8("lblLed"))
        self.rb0 = QtGui.QRadioButton(self.frame)
        self.rb0.setGeometry(QtCore.QRect(460, 10, 41, 21))
        self.rb0.setObjectName(_fromUtf8("rb0"))
        self.rb0.setChecked(True)
        self.rb0.toggled.connect(lambda:self.rbstate(self.rb0))
        self.rb25 = QtGui.QRadioButton(self.frame)
        self.rb25.setGeometry(QtCore.QRect(510, 10, 51, 21))
        self.rb25.setObjectName(_fromUtf8("rb25"))
        self.rb25.toggled.connect(lambda:self.rbstate(self.rb25))
        self.rb50 = QtGui.QRadioButton(self.frame)
        self.rb50.setGeometry(QtCore.QRect(570, 10, 51, 21))
        self.rb50.setObjectName(_fromUtf8("rb50"))
        self.rb50.toggled.connect(lambda:self.rbstate(self.rb50))
        self.rb75 = QtGui.QRadioButton(self.frame)
        self.rb75.setGeometry(QtCore.QRect(630, 10, 51, 21))
        self.rb75.setObjectName(_fromUtf8("rb75"))
        self.rb75.toggled.connect(lambda:self.rbstate(self.rb75))
        self.rb100 = QtGui.QRadioButton(self.frame)
        self.rb100.setGeometry(QtCore.QRect(690, 10, 100, 21))
        self.rb100.setObjectName(_fromUtf8("rb100"))
        self.rb100.toggled.connect(lambda:self.rbstate(self.rb100))
        self.rbFull = QtGui.QRadioButton(self.frame)
        self.rbFull.setGeometry(QtCore.QRect(630, 50, 100, 21))
        self.rbFull.setObjectName(_fromUtf8("brFull"))
        self.rbFull.toggled.connect(lambda:self.rbstate(self.rbFull))
        self.lblEixo_x = QtGui.QLabel(self.centralWidget)
        self.lblEixo_x.setGeometry(QtCore.QRect(670, 520, 111, 16))
        self.lblEixo_x.setObjectName(_fromUtf8("lblEixo_x"))
        self.lblEixo_y = QtGui.QLabel(self.centralWidget)
        self.lblEixo_y.setGeometry(QtCore.QRect(20, 520, 211, 20))
        self.lblEixo_y.setObjectName(_fromUtf8("lblEixo_y"))
        #Form.setCentralWidget(self.centralWidget)
        self.menuBar = QtGui.QMenuBar(Form)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 800, 20))
        self.menuBar.setObjectName(_fromUtf8("menuBar"))
        #Form.setMenuBar(self.menuBar)
        self.mainToolBar = QtGui.QToolBar(Form)
        self.mainToolBar.setObjectName(_fromUtf8("mainToolBar"))
        #Form.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtGui.QStatusBar(Form)
        self.statusBar.setObjectName(_fromUtf8("statusBar"))
        #Form.setStatusBar(self.statusBar)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)
    
    
    def conectar(self):
        if unicode(self.comboBox.currentText()) != "":
            raw = serial.Serial(unicode(self.comboBox.currentText()), 9600)
            if self.btnConectar.isChecked() == True:
                if raw._isOpen == True:
                    raw.close()
                    raw.open()
                else:
                    raw.open()
                raw.write(b'E')
                self.btnConectar.setText("Finalizar")
            if self.btnConectar.isChecked() == False:
                if raw._isOpen:
                    raw.close()
                self.btnConectar.setText("Iniciar")
        else:
            raw = 0
        
        
    def rbstate(self,b):
        raw = serial.Serial(unicode(self.comboBox.currentText()), 9600)
        if b.text() == "0%":
            raw.write(b'F')
        elif b.text() == "25%":
            raw.write(b'W')
        elif b.text() == "50%":
            raw.write(b'X')
        elif b.text() == "75%":
            raw.write(b'Y')
        elif b.text() == "100%":
            raw.write(b'Z')
    
    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Aquisição de Dados em Tempo Real", None))
        self.label.setText(_translate("Form", "Porta Serial:", None))
        self.btnConectar.setText(_translate("Form", "Iniciar", None))
        self.label_2.setText(_translate("Form", "Intensidade luminosa:", None))
        self.label_3.setText(_translate("Form", "Leitura do fotodiodo:", None))
        self.rb0.setText(_translate("Form", "0%", None))

        self.rb25.setText(_translate("Form", "25%", None))
        self.rb50.setText(_translate("Form", "50%", None))
        self.rb75.setText(_translate("Form", "75%", None))
        self.rb100.setText(_translate("Form", "100%", None))
        self.rbFull.setText(_translate("Form", "0 a 100%", None))
        self.lblEixo_x.setText(_translate("Form", "Eixo x: Tempo(ms)", None))
        self.lblEixo_y.setText(_translate("Form", "Eixo y: Intensidade luminosa (lux)", None))

from pyqtgraph import PlotWidget