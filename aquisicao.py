#!/usr/bin/python
# -*- coding: utf-8 -*-

import pyqtgraph as pg
import numpy as np
import serial.tools.list_ports

from pyqtgraph.Qt import QtGui, QtCore
from pyqtgraph.ptime import time
from AquisicaoTemplate_pyqt import Ui_Form
from serial.serialutil import SerialException
from colorama.ansi import Style
from reportlab.lib.pdfencrypt import padding

## Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])

win = QtGui.QWidget()
win.setWindowTitle(u'Aquisição de Dados em Tempo Real')
ui = Ui_Form()
ui.setupUi(win)
win.show()

p = ui.plot
p.setRange(yRange=[0,1023], xRange=[0,100], padding=0)
data = [0]

try:
    ui.comboBox.insertItems(0,[port[0] for port in serial.tools.list_ports.comports() if port[2] != 'n/a'])
    raw = serial.Serial(unicode(ui.comboBox.currentText()), 9600)
except SerialException:
    raw = 0  


def update():
    global curve, data, raw, xdata
    try:
        if raw._isOpen and ui.btnConectar.isChecked():
            raw.write(b'E')
            line = raw.readline()
            data.append(int(line))
            xdata = np.array(data, dtype='int')
            curve = pg.PlotDataItem(y = xdata, 
                               pen=pg.mkPen(color=(52, 152, 219),
                                            width=1.5)
                                    )
            p.addItem(curve)
            ui.lblFoto.setText(line)
            app.processEvents()  ## force complete redraw for every plot
            
    except SerialException:
        raw = 0

timer = QtCore.QTimer()
timer.timeout.connect(update)
timer.start(1000)
        
## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()